# ref:
# https://innovation.staffr.net/rest/v1/2020-01-01/core/entities#tag/clients
module TrackTik
  class Core
    module Client
      URI = '/clients'
      #Retrieve a Client/Site item using its unique identifier.
      def retrieve_client(id, options={})
        if id
          response = self.class.get("#{URI}/#{id}", { query: options })
          error_handler(response)
          hash_response response.body
        end
      end

      # Retrieve a collection of Clients/Sites.
      # You can search and filters the collection using query parameters.
      def retrieve_clients(options={})
        response = self.class.get(URI, { query: options })
        error_handler(response)
        hash_response response.body
      end

      # Store a new Client/Site item.
      def create_client(options)
        response = self.class.post(URI, body: options )
        error_handler(response)
        hash_response response.body
      end

      #Edit a Client item completely.
      def update_client(client_id, options)
        response = self.class.put("#{URI}/#{client_id}", body: options )
        error_handler(response)
        hash_response response.body
      end


    end
  end
end
