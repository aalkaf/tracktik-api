require ::File.expand_path("../../test_helper", __dir__)

module TrackTik
  class ClientTest < Minitest::Test
    describe ".retrieve_clients" do
      it 'shows all clients in the system' do
        response = @@core.retrieve_clients
        refute_nil response
        assert response[:data].instance_of?(Array), true
      end

      it 'shows only two clients' do
        response = @@core.retrieve_clients({limit: 2})
        refute_nil response
        assert response[:data].instance_of?(Array), true
        assert response.dig(:data).size, 2
      end

      it 'shows a client given an id' do
      end
    end
  end
end
