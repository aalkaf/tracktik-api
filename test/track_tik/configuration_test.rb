require ::File.expand_path("../test_helper", __dir__)

module TrackTik
  class ConfigurationTest < Minitest::Test
    describe "initialization" do
      it 'sets defaluts value for api key, client_id, client_secret, username, password, scope' do
        configuration = TrackTik::Configuration.new
        assert_equal configuration.server_url ,'PLEASE PROVIDE SERVER URL'
        assert_equal configuration.client_id ,'PLEASE PROVIDE CLIENT ID'
        assert_equal configuration.client_secret ,'PLEASE PROVIDE CLIENT SECRET'
        assert_equal configuration.username ,'PLEASE PRVOIDE USER NAME'
        assert_equal configuration.password ,'PLEASE PROVIDE PASSWORD'
        assert_equal configuration.scope ,'PLEASE PROVIDE SCOPE'
      end
    end
  end
end
