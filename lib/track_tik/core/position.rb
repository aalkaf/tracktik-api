# reference:
# https://innovation.staffr.net/rest/v1/2020-01-01/core/entities#tag/positions

module TrackTik
  class Core
    module Position
      URI = '/positions'
      # Retrieve a collection of Positions.
      # You can search and filters the collection using query parameters.
      # https://innovation.staffr.net/rest/v1/2020-01-01/core/entities#tag/positions
      def retrieve_positions(options={})
        response = self.class.get(URI, { query: options })
        error_handler(response)
        hash_response response.body
      end

      # Retrieve a Position item using its unique identifier.
      def retrieve_position(id, options={})
        if id
          response = self.class.get("#{URI}/#{id}", { query: options })
          error_handler(response)
          hash_response response.body
        end
      end

      # Store a new Position item.
      def create_position(options)
        response = self.class.post(URI, body: options)
        error_handler(response)
        hash_response response.body
      end

      #Edit a Position item completely.
      def update_position(id, options)
        response = self.class.put("#{URI}/#{id}", body: options)
        error_handler(response)
        hash_response response.body
      end

    end
  end
end
