# ref:
# https://innovation.staffr.net/rest/v1/2020-01-01/core/entities#tag/accounts
module TrackTik
  class Core
    module Account
      URI ='/accounts'

      # Retrieve a collection of Accounts.
      # You can search and filters the collection using query parameters.
      def retrieve_accounts(options={})
        response = self.class.get(URI, { query: options })
        error_handler(response)
        hash_response response.body
      end

      # Retrieve a Account item using its unique identifier.
      def retrieve_account(account_id)
        raise "Account id is rquired" if account_id.nil?
        response = self.class.get("#{URI}/#{account_id}")
        error_handler(response)
        hash_response response.body
      end

    end
  end
end
