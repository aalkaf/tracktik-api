require ::File.expand_path("../../test_helper", __dir__)

module TrackTik
  class PositionTest < Minitest::Test

    describe ".retrieve_positions" do
      it 'shows all positions in the system' do
        response = @@core.retrieve_positions
        refute_nil response
        assert response[:data].instance_of?(Array), true
      end

      it 'shows only two positions' do
        response = @@core.retrieve_positions({limit: 2})
        refute_nil response
        assert response[:data].instance_of?(Array), true
        assert response.dig(:data).size, 2
      end

      it 'shows an account given an id' do
        position_id = 1
        response = @@core.retrieve_position(position_id)
        refute_nil response
        refute_nil response.dig(:data)
      end
    end


  end
end
