module TrackTik
  class Error < StandardError
    attr_reader :message

    # Response obtained from tracktik server
    attr_accessor :response

    attr_reader :code
    attr_reader :http_status
    attr_reader :http_body

    def initialize(message: nil, code: nil, http_status: nil, http_body: nil )
      @message = message
      @code    = code
      @http_status = http_status
      @http_body = http_body
    end



  end
end
