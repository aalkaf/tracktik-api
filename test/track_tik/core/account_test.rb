require ::File.expand_path("../../test_helper", __dir__)

module TrackTik
  class AccountTest < Minitest::Test

    describe ".retrieve_accounts" do
      it 'shows all accounts in the system' do
        response = @@core.retrieve_clients
        refute_nil response
        assert response[:data].instance_of?(Array), true
      end

      it 'shows only two accounts' do
        response = @@core.retrieve_accounts({limit: 2})
        refute_nil response
        assert response[:data].instance_of?(Array), true
        assert response.dig(:data).size, 2
      end

      it 'shows an account given an id' do
        account_id = 1
        response = @@core.retrieve_accounts(account_id)
        refute_nil response
        refute_nil response.dig(:data)
      end
    end

  end
end
