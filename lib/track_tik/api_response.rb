module TrackTik
  module ApiResponse

    # Given responses body, parse the body and return hash that is symbolized
    def hash_response(response_body)
      JSON.parse(response_body, symbolize_names: true)
    end

    # Given the response from httpparty request, we raise an exception
    def error_handler(response)
      case response.code
      when 400..600
        raise TrackTik::Error.new(
          message: response["message"],
          code: response.code,
          http_status: response.code,
          http_body: hash_response(response.body),
        )
      end
    end

  end
end
