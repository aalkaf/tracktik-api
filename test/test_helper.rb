# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path("../lib", __dir__)
require "track_tik"

require "minitest/autorun"

module Minitest
  class Test
    def setup
     TrackTik.configure do |config|
       config.api_uri = 'https://smartparking.guards.app/rest/v1/'
       config.client_id = ENV['tracktik_client_id']
       config.client_secret = ENV['tracktik_client_secret']
       config.username = ENV['tracktik_username']
       config.password = ENV['tracktik_password']
       config.server_url = 'https://smartparking.guards.app'
       config.scope = 'all:read all:write'
     end
     @@core = TrackTik.core
    end
  end
end
