require ::File.expand_path("../../test_helper", __dir__)

module TrackTik
  class ReportFieldTest < Minitest::Test
    # @@core = TrackTik.core
    describe ".retrieve_report_field" do

      it 'shows a report field given an id' do
        report_field_id = 1
        response = @@core.retrieve_report_field(1)
        refute_nil response
        assert_equal response.dig(:data, :id), report_field_id
      end

      it 'raises error when id of report field is not found' do
        assert_raises TrackTik::Error do
          @@core.retrieve_report_field(7777777777)
        end
      end

    end

    describe ".retrieve_report_fields" do
      it 'shows list of report fields' do
        response = @@core.retrieve_report_fields
        refute_nil response
        refute_nil response[:data]
        assert response[:data].instance_of?(Array), true
      end
    end


    describe ".create_report_field" do
      it 'creates new report field' do
        # report_templates = @@core.retrieve_report_templates
        # report_tempplate_id = report_templates[:data][0][:id]


        ops = {
          report: 150,
          templateField: 74,
          value: 12,
          archived: false,
        }
        response = @@core.create_report_field(ops)
        refute_nil response
        refute_nil response[:data]
      end
    end

    describe ".update_report_field" do
      it 'updates existing report field' do
        # report_templates = @@core.retrieve_report_templates
        # report_tempplate_id = report_templates[:data][0][:id]
        report_field_id = 253

        ops = {
          value: 12,
          archived: false,
        }
        response = @@core.update_report_field(report_field_id, ops)
        refute_nil response
        refute_nil response[:data]
      end
    end


  end
end
