# TrackTik

A Ruby interface to the TrackTik API.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'track_tik'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install track_tik

## Configuration

TrackTik API v1.0 requires you to authenticate via OAuth, so you would need to
register your application with TrackTik.

Your new application will be assigned a client_id, client_secret, username,
and password. You would need to configure these values before you make a
request or else you will get an error Unauthroized.

You can pass configuration options as a bock to TrackTik as following

``` ruby
TrackTik.configure do |config|
  config.api_uri = 'YOUR API URL'
  config.client_id = "YOUR CLIENT ID"
  config.client_secret = "YOUR CLIENT SECRET "
  config.username = "YOUR USERNAME"
  config.password = "YOUR PASSWORD"
  config.server_url = 'YOUR SERVER URL'
  config.scope = 'YOUR SCOPE'
end
```
Example of api_uri & server_url
api_uri = 'https://smartparking.guards.app/rest/v1/'
server_url = 'https://smartparking.guards.app'
scope = 'all:read all:write'

After setting up the configuration, you would need pass access token to the core  

```ruby
core = TrackTik.core(access_token)
```
To get access token, you can use method:
```ruby  
response = TrackTik.authenticate
access_token = response.dig(:access_token)
```
## Usage

You would need to refer to https://innovation.staffr.net/rest/v1 for more information
about what each endpoint accepts

After configuring a core, you can do the following things:

### Clients
```ruby
TrackTik.retrieve_clients({limit: 10})
TrackTik.retrieve_client({id: 1})
TrackTik.create_client({})
TrackTik.update_client({})
```
### Report Template
```ruby
TrackTik.retrieve_report_templates({limit: 10})
TrackTik.retrieve_report_template(id, options={})
TrackTik.create_report_template(options)
```
So far only 5 resources can be used:
  - client
  - report_template
  - report_template_field
  - report
  - report_field

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/track_tik. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://github.com/[USERNAME]/track_tik/blob/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the TrackTik project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/track_tik/blob/master/CODE_OF_CONDUCT.md).
