require ::File.expand_path("../test_helper", __dir__)

module TrackTik
  class AuthenticationTest < Minitest::Test
    describe ".oauth_tokens" do
      it 'return access token and refersh token' do
        response = TrackTik.authenticate
        refute_nil response.dig(:token_type)
        assert_equal response.dig(:token_type), 'Bearer'
        assert_equal response.dig(:expires_in), 3600
        refute_nil response.dig(:refresh_token)
      end
    end
  end
end
