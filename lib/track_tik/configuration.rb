module TrackTik

  class Configuration
    attr_accessor :api_uri
    attr_accessor :server_url
    attr_accessor :client_id
    attr_accessor :client_secret
    attr_accessor :username
    attr_accessor :password
    attr_accessor :scope

    def initialize
      @api_uri = 'PLEASE PROVIDE API URL'
      @client_id  = 'PLEASE PROVIDE CLIENT ID'
      @client_secret = 'PLEASE PROVIDE CLIENT SECRET'
      @username   = 'PLEASE PRVOIDE USER NAME'
      @password   = 'PLEASE PROVIDE PASSWORD'
      @scope      = 'PLEASE PROVIDE SCOPE'
      @server_url    = 'PLEASE PROVIDE SERVER URL'

    end


  end
end
