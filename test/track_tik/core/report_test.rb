require ::File.expand_path("../../test_helper", __dir__)

module TrackTik
  class ReportTest < Minitest::Test

    # @@core = TrackTik::Core.new

    describe ".retrieve_reports" do

      it 'shows list of reports in the system given at least one filter(id)' do
        response = @@core.retrieve_reports(id: 92)
        refute_nil response
        refute_nil response[:data]
        assert response[:data].instance_of?(Array), true
      end

      it 'should throw exception when no filters provided' do
        assert_raises TrackTik::Error do
          @@core.retrieve_reports
        end
      end

    end

    describe ".retrive_report" do
      it 'shows a report' do
        id = 92
        response = @@core.retrive_report(id)
        refute_nil response
        refute_nil response[:data]
        assert_equal response[:data][:id], id
      end

      it 'throws error when report is not found' do
        id = 88888888888888888
        assert_raises TrackTik::Error do
          @@core.retrive_report(id)
        end
      end

      describe ".create_report" do
        it 'creates report' do
          # report_templates = @@core.retrieve_report_templates
          # report_template_id = report_templates.dig(:data, 0, :id)
          report_template_id = 70

          ops = {
            reportTemplate: report_template_id,
            status: "NEW",
            account: 4,
            position: 1,
            reportFields: [],
          }

          response = @@core.create_report(ops)

          refute_nil response
          refute_nil response.dig(:data)
          refute_nil response.dig(:data, :id)
        end

        describe ".update report" do
          it 'updates report' do
            report_templates = @@core.retrieve_report_templates
            report_tempplate_id = report_templates.dig(:data, 0, :id)
            report_id = 92
            ops = {
              reportTemplate: report_tempplate_id,
              reportDateTime: Time.now,
              submittedOn: Time.now,
              status: "APPROVED",
              account: 4,
              position: 1
            }
            response = @@core.update_report(report_id, ops)

            refute_nil response
            refute_nil response.dig(:data)
          end
        end
      end
    end

  end

end
