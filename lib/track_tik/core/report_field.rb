# ref:
# https://innovation.staffr.net/rest/v1/2020-01-01/core/reporting#tag/report-fields
module TrackTik
  class Core
    module ReportField
      URI = '/report-fields'

      #Retrieve a Report Field item using its unique identifier.
      def retrieve_report_field(report_field_id)
        if report_field_id
          response = self.class.get("#{URI}/#{report_field_id}")
          error_handler(response)
          hash_response response.body
        end
      end

      # Retrieve a collection of Report Fields.
      # You can search and filters the collection using query parameters.
      def retrieve_report_fields(options={})
        response = self.class.get(URI, { query: options } )
        error_handler(response)
        hash_response response.body
      end

      # Store a new Report Field item.
      def create_report_field(options)
        response = self.class.post(URI, body: options )
        error_handler(response)
        hash_response response.body
      end

      # Edit a Report Field item completely. To only update some attributes, use the PATCH operation.
      def update_report_field(report_id, options)
        response = self.class.put("#{URI}/#{report_id}", body: options )
        error_handler(response)
        hash_response response.body
      end


    end
  end
end
