require_relative "error"
require_relative "api_response"
require_relative "core/client"
require_relative "core/report_template"
require_relative "core/report_template_field"
require_relative "core/report"
require_relative "core/report_field"
require_relative "core/account"
require_relative "core/position"

module TrackTik
  class Core

    attr_accessor :access_token
    include HTTParty
    # Handler for formmting of responses
    include ApiResponse

    # Resources (Core resources)
    include TrackTik::Core::Client
    include TrackTik::Core::ReportTemplate
    include TrackTik::Core::ReportTemplateField
    include TrackTik::Core::Report
    include TrackTik::Core::ReportField
    include TrackTik::Core::Account
    include TrackTik::Core::Position

    format :json

    def initialize(access_token = nil)
      @access_token =  access_token || ENV["TRACKTIK_ACCESS_TOKEN"]
      self.class.base_uri(TrackTik.configuration.api_uri)
      self.class.default_options.merge!(
        headers: {
          'Authorization' => "Bearer #{@access_token}",
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      )
    end


  end
end
