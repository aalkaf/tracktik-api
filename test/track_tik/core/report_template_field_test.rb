require ::File.expand_path("../../test_helper", __dir__)

module TrackTik
  class ReportTemplateFieldTest < Minitest::Test
    # @@core = TrackTik::Core.new

    describe ".retrieve_report_template_field" do

      it 'shows a report template field given an id' do
        response = @@core.retrieve_report_template_field(8)
        refute_nil response
        refute_nil response
      end

      it 'shows error when id is not found' do
        assert_raises TrackTik::Error do
          @@core.retrieve_report_template_field(7777777777)
        end
      end

    end
    #
    describe ".retrieve_report_template_fields" do
      it 'shows list of report template fields' do
        response = @@core.retrieve_report_template_fields
        refute_nil response
        refute_nil response[:data]
        assert response[:data].instance_of?(Array), true
      end
    end

    describe ".create_report_template_field" do
      it 'creates new report template field' do
        report_templates = @@core.retrieve_report_templates
        report_tempplate_id = report_templates[:data][0][:id]


        ops = {
          label: "citations#",
          type: "TEXT",
          required: true,
          adminOnly: false,
          displayOrder: 1,
          extra: "Tag",
          reportTemplate: report_tempplate_id
        }
        response = @@core.create_report_template_field(ops)
        refute_nil response
        refute_nil response[:data]
      end
    end
  end
end
