# frozen_string_literal: true

require "httparty"

# Version
require_relative "track_tik/version"

# Configuration
require_relative "track_tik/configuration"
require_relative "track_tik/authentication"

# Core that has all resources
require_relative "track_tik/core"

# Error Handler
require_relative "track_tik/error"


module TrackTik
  class << self
    attr_accessor :configuration
  end

  #Return the configuration class instance if block given
  def self.configure
    self.configuration ||= ::TrackTik::Configuration.new
    yield(configuration) if block_given?
  end

  def self.core(access_token = nil)
    TrackTik::Core.new(access_token)
  end

  def self.authenticate
    TrackTik::Authentication.new.oauth_tokens
  end
end

TrackTik.configure unless TrackTik.configuration
