module TrackTik
  class Core
    module Report
      URI = '/reports'
      #Retrieve a collection of Reports.
      #You can search and filters the collection using query parameters.
      # @param filters, at least one of the following filters must be provided
      # reportDateTime, submittedOn, position, approvedOn, id
      def retrieve_reports(options={})
        if options
          response = self.class.get(URI, { query: options })
          # when all mandatroy fields are missing , for example.
          error_handler(response)

          hash_response response.body
        end
      end

      # options: query params
      # id : id of the report
      def retrive_report(id, options={})
        if id
          response = self.class.get("#{URI}/#{id}", { query: options })
          error_handler(response)
          hash_response response.body
        end
      end

      def create_report(options)
        response = self.class.post(URI, body: options)
        error_handler(response)
        hash_response response.body
      end

      def update_report(id, options)
        response = self.class.put("#{URI}/#{id}", body: options)
        error_handler(response)
        hash_response response.body
      end

    end
  end
end
