# ref:
# https://innovation.staffr.net/rest/v1/2020-01-01/core/reporting#tag/report-templates
module TrackTik
  class Core
    module ReportTemplate
      URI = '/report-templates'

      # Retrieve a collection of Report Templates.
      # You can search and filters the collection using query parameters.
      def retrieve_report_templates(options={})
        response = self.class.get(URI, { query: options })
        error_handler(response)
        hash_response response.body
      end

      # Retrieve a Report Template item using its unique identifier.
      def retrieve_report_template(id, options={})
        response = self.class.get("#{URI}/#{id}")
        error_handler(response)
        hash_response response.body
      end

      #Store a new Report Template item.
      def create_report_template(options)
        response = self.class.post(URI, body: options)
        error_handler(response)
        hash_response response.body
      end

      # Edit a Report Template item completely.
      # To only update some attributes, use the PATCH operation.
      def update_report_template(report_id, options={})
        response = self.class.put("#{URI}/#{report_id}", body: options)
        error_handler(response)
        hash_response response.body
      end

    end
  end
end
