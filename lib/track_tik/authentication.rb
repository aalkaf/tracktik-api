module TrackTik
  class Authentication
    attr_accessor :options
    attr_accessor :auth_full_url

    def initialize
      @auth_full_url = "#{TrackTik.configuration.server_url}/rest/oauth2/access_token"
      @options = {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        body: {
          'grant_type' => 'password',
          'client_id': TrackTik.configuration.client_id,
          'client_secret' => TrackTik.configuration.client_secret,
          'username' => TrackTik.configuration.username,
          'password' =>  TrackTik.configuration.password,
          'scope' => TrackTik.configuration.scope
        },
        url: auth_full_url,
      }
    end

    # Returns a symbolized hash that include
    # :token_type, :expires_in, :access_token, refresh_token
    def oauth_tokens
      response = HTTParty.post(auth_full_url, options)
      json_response = JSON.parse(response.body, symbolize_names: true)
      return json_response
    end



  end
end
