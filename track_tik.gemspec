# frozen_string_literal: true

require_relative "lib/track_tik/version"

Gem::Specification.new do |spec|
  spec.name          = "track_tik"
  spec.version       = TrackTik::VERSION
  spec.authors       = ["Abdullah Alkaf"]
  spec.email         = ["aalkaf@smartbuildingapps.com"]

  spec.summary       = "Ruby bindings for the TrackTik API"
  spec.description   = "TrackTik connects your frontline, back office management, and customers in one place to bring fluidity to your security operations."
  spec.homepage      = "https://innovation.staffr.net/rest/v1"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.4.0")

  spec.metadata["allowed_push_host"] = "https://innovation.staffr.net/rest/v1"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://github.com"
  spec.metadata["changelog_uri"] = "https://github.com"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.glob("{bin,lib}/**/*")
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.add_runtime_dependency "httparty", "~> 0.13.5"
  # Uncomment to register a new dependency of your gem
  # spec.add_dependency "example-gem", "~> 1.0"

  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
